<?php
//модуль вывода списка заявок
function getUsers($status, $user){
if ($status) $result = $user->getAuthors();
else $result = $user->getCandidates();

if($result === false){
	$errMsg = "Произошла ошибка при выводе списка пользователей";
	exit;
}else{?>
	<table border="1" cellpadding="5" cellspacing="0" width="100%">
		<tr>
			<th>Имя</th>
			<th>email</th>
			<th>Организация</th>
			<th>Телефон</th>
		</tr>
	<?foreach($result as $item){
		$id = $item['id'];
		$email = $item['email'];
		$uName = $item['uName'];
		$uOrganization = $item['uOrganization'];
		$phone = $item['phone'];?>
		<tr>
			<td><?=$uName?></td>
			<td><?=$email?></td>
			<td><?=$uOrganization?></td>
			<td><?=$phone?></td>
			<td><a href="<?
            if($status)
                echo "{$_SERVER['PHP_SELF']}?del=$id&status=1";
            else
                echo "{$_SERVER['PHP_SELF']}?del=$id&status=0";
            ?>">Удалить</a></td>
            <?if($status === 0){
                ?>
                <td><a href="#" id="<?=$id?>" email="<?=$email?>" onclick="setStatus(this);">К регистрации</a></td>
            <?}?>
		</tr>
		
	<?}?>
	</table>
<?}
}
?>
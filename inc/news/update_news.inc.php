<?php //модуль изменения новости
$id = $_POST['artId']*1;
$title = $news->clearData($_POST['title']);
$desc = $news->clearData($_POST['description']);
$category = $_POST['category']*1;
if(empty($title) or empty($desc) or $id===0 or $category===0){
	$errMsg = 'FIELDS!';
}else{
	$ret = $news->updateNews($id, $title, $category, $desc);
	if(!$ret){
		$errMsg = 'Save error!';
	}else{
		header('Location: ' . $_SERVER['PHP_SELF']);
		exit;
	}
}
?>
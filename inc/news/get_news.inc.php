<?php
//модуль вывоа новостной ленты
$result = $news->getNews();
if($result === false){
	$errMsg = "Произошла ошибка при выводе новостной ленты";
}else{
	foreach($result as $item){
		$id = $item['id'];
		$title = $item['title'];
		$category = $item['category'];
		$description = nl2br($item['description']);
		$datetime = date("d-m-Y H:i:s",$item['datetime']);
        $userId = $item['userId'];
        if ($userId==0){
            $authorName = 'Админ';
        } else{
            $authorName = $authors->getAuthorById($userId);
            $authorName = $authorName[0]['uName'];
        }

        echo <<<LABEL
		<hr>
		<article>
			<h4 class="ttl" id="$id"><a href="#$id" onclick="openDesc(this)">$title</a> 
            <span style="font-weight: normal;">[<span class="ctg">$category</span>] @ <span class="dt">$datetime</span> | $authorName</span> </h4> 
			<p class="dsc" style="display: none;">$description</p>
LABEL;
		if(isset($_SESSION['admin']) and ($_SESSION['admin'] or $userId==$authorArr[0]['id']))
            echo <<<LABEL
            <p align="right">
                <a href="{$_SERVER['PHP_SELF']}?del=$id">Удалить</a>
                <a href="#" onclick="goToUpdate(this)" id="$id">Редактировать</a>
            </p>
LABEL;
  echo '</article>';
	}
}
?>
<?php
include_once __DIR__ .'/url.php';
function smtp_send_mail($uri, $data, $flag, $context)
{
	$uri = new uri($uri);
	
	$smtp = fsockopen($uri->host, $uri->port, $errno, $errstr, 10);
	if(!$smtp)
		die("соединение с сервером не прошло");
	$response = fgets($smtp, 512);
	//echo "$response\n";
	
	fputs($smtp,"EHLO kurokami\r\n");
	do
	{
		$response = fgets($smtp);
		//echo "$response\n";
		if(substr($response, 0, 3) != "250")
			die("ошибка приветсвия ELHO");
	}
	while(substr($response, 3, 1) != " ");
		
	fputs($smtp,"AUTH LOGIN\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response, 0, 3) != 334) 
		die("сервер не разрешил начать авторизацию");
		
	fputs($smtp,base64_encode($uri->user)."\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response, 0, 3) != 334) 
		die("ошибка доступа к такому юзеру");
		
	fputs($smtp,base64_encode($uri->pass)."\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response, 0, 3) != 235) 
		die("не правильный пароль");
	
	if(!isset($context["envelope"]))
		return false;
	if(!isset($context["envelope"]["from"]) || !isset($context["envelope"]["to"]))
		return false;
	if(!isset($context["envelope"]["custom_headers"]))
		$context["envelope"]["custom_headers"] = array();
	$context["envelope"]["custom_headers"] = array_merge($context["envelope"]["custom_headers"], array("User-Agent: Kurokami Web Mail"));

	$part1["type"] = TYPETEXT;
	$part1["subtype"] = "plain";
	$part1["charset"] = "UTF-8";
	$part1["contents.data"] = $data;
	$parts[] = $part1;
	
	if(isset($context["attachment"]) && !empty($context["attachment"]))
	{
		$part2["type"] = TYPEMULTIPART;
		$part2["subtype"] = "mixed";
		array_unshift($parts, $part2);
		foreach($context["attachment"] as $fname)
		{
			$file = fopen($fname, "r");
			$contents = fread($file, filesize($fname));
			fclose($file);
			
			$part3["type"] = TYPEAPPLICATION;
			$part3["encoding"] = ENCBINARY;
			$part3["subtype"] = mime_type($fname);
			$part3["description"] = basename($fname);
			$part3['disposition.type'] = 'attachment';
			$part3['disposition'] = array ('filename'=>basename($fname));
			$part3["contents.data"] = $contents;
			$parts[] = $part3;
		}
	}

	$data = imap_mail_compose($context["envelope"], $parts);
	//echo $data;
	$size = strlen($data);

	fputs($smtp,"MAIL FROM:<{$context["envelope"]["from"]}> SIZE=$size\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response,0,3) != 250) 
		die("сервер отказал в команде MAIL FROM");
	
	fputs($smtp,"RCPT TO:<{$context["envelope"]["to"]}>\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response,0,3) != 250 && substr($response,0,3) != 251) 
		die("Сервер не принял команду RCPT TO");
	
	fputs($smtp,"DATA\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response,0,3) != 354) 
		die("сервер не принял DATA");
	
	fputs($smtp,"$data\r\n.\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
	if(substr($response,0,3) != 250) 
		die("ошибка отправки письма");
	list(,$id) = explode("=", $response);
	
	fputs($smtp,"QUIT\r\n");
	$response = fgets($smtp);
	//echo "$response\n";
		
	fclose($smtp);
	return $id;
}
/*
echo "<pre>\n";
$res = smtp_send_mail(
	"smtp://me@kurokami.ru:lsd666999@mail.kurokami.ru:25",
	"Dear Me! I glad to see you!",
	null,
	array(
		"envelope" => array(
			"from" => "me@kurokami.ru",
			"to" => "mvInteh@yandex.ru",
			"subject" => "about kurokami web mail"
		),
		"attachment" => array(
			"sendmail.php"
		)
	)
);
echo "The id of mail id $res\n";
echo "</pre>\n";
*/
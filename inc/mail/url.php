<?php
function parse_any($str, $delimiter = "&", $separator = "=")
{
	$res = array();
	$arr = explode($delimiter, $str);
	for($i = 0; $i < count($arr); $i++)
	if(!empty($arr[$i]))
	{	
		$el = explode($separator, $arr[$i], 2);
		$res[$el[0]] = isset($el[1]) ? $el[1] : null;
	}
	return $res;
}

function build_any($arr, $delimiter = "&", $separator = "=", $quot = false)
{
	$res = "";
	foreach($arr as $key=>$val)
	{
		$res .= (!empty($res) ? $delimiter : "") . $key . 
			(!empty($val) ? $separator . ($quot ? "'$val'" : $val) : "");
	}
	return $res;
}

function http_parse_uri($url)
{
	$url = parse_url($url);
	if(isset($url["path"]))
		$url["path"] = pathinfo($url["path"]);
	if(isset($url["path"]["dirname"]) && (substr($url["path"]["dirname"],0,1) == "/" || substr($url["path"]["dirname"],0,1) == "\\"))
		$url["path"]["dirname"] = substr($url["path"]["dirname"],1);
	if(isset($url["query"]))
		$url["query"] = parse_any($url["query"]);
	return $url;
}

function http_build_uri($url)
{
	if(isset($url["query"]))
		$query = build_any($url["query"]);
	$path = "/";
	if(!empty($url["path"]["dirname"]) && $url["path"]["dirname"] != ".")
		$path .= $url["path"]["dirname"];
	if(!empty($url["path"]["basename"]))
		$path .= (!empty($url["path"]["dirname"]) ? "/" : "") . $url['path']['basename'];
	return (!empty($url["scheme"]) ? "{$url['scheme']}://" : "") .
			(!empty($url["user"]) ? ($url["user"] . (!empty($url["pass"]) ? (":" . $url["pass"]) : "") . "@") : "") .
			(!empty($url["host"]) ? ($url["host"] . (!empty($url["port"]) ? (":" . $url["port"]) : "")) : "") .
			$path . 
			(!empty($query) ? "?$query" : "") .
			(!empty($url["fragment"]) ? "#{$url['fragment']}" : "");
}

function mime_type($url)
{
	$ext = pathinfo($url, PATHINFO_EXTENSION);
	switch($ext)
	{
	case "htm":
	case "html":
		return "text/html"; 
	case "xml":
		return "text/xml"; 
	case "xsl":
	case "xslt":
		return "text/xslt";
	case "php":
	case "phtml":
		return "application/php";
	case "css":
		return "text/css"; 
	case "txt":
		return "text/plain"; 
	case "js":
		return "text/javascript";
	case "ico":
		return "image/x-icon";
	case "gif":
		return "image/gif"; 
	case "jpg":
	case "jpeg":
		return "image/jpeg";
	case "png":
		return "image/png"; 
	case "pdf":
		return "application/pdf";
	case "json":
		return "application/json";
	}
	return false;
}

class uri_path extends ArrayObject
{
	function __construct($path)
	{
		parent::__construct(pathinfo($path));
		if(isset($this["dirname"]) && (substr($this["dirname"],0,1) == "/" || substr($this["dirname"],0,1) == "\\"))
			$this["dirname"] = substr($this["dirname"],1);
	}
    public function &__get ($key) 
	{
		$ret = $this[$key];
        return $ret;
    }
    public function __set($key,$value) 
	{
        $this[$key] = $value;
    }
    public function __isset ($key) 
	{
        return isset($this[$key]);
    }
    public function __unset($key) 
	{
        unset($this[$key]);
    }
	function __toString()
	{
		$path = "/";
		if(!empty($this["dirname"]) && $this["dirname"] != ".")
			$path .= $this["dirname"];
		if(!empty($this["basename"]))
			$path .= (!empty($this["dirname"]) ? "/" : "") . $this['basename'];
		return $path;
	}
}

class uri_query extends ArrayObject
{
	function __construct($query)
	{
	parent::__construct(parse_any($query));
	}
	public function &__get($key) 
	{
	$ret = $this[$key];
			return $ret;
	}
	public function __set($key,$value) 
	{
			$this[$key] = $value;
	}
	public function __isset($key) 
	{
			return isset($this[$key]);
	}
	public function __unset($key) 
	{
			unset($this[$key]);
	}
	function __toString()
	{
		return build_any($this);
	}
}

class uri extends ArrayObject
{
	function __construct($uri)
	{
		parent::__construct(parse_url($uri));
		$this["path"] = new uri_path(isset($this["path"]) ? $this["path"] : "");
		$this["query"] = new uri_query(isset($this["query"]) ? $this["query"] : "");
	}
	public function &__get($key) 
	{
		//if(isset($this[$key]))
		//{
			$ret = null;
			if(isset($this[$key]))
			$ret = $this[$key];
			return $ret;
		//}
		//return "";
	}
	public function __set($key,$value) 
	{
		$this[$key] = $value;
	}
	public function __isset($key) 
	{
		return isset($this[$key]);
	}
	public function __unset($key) 
	{
		unset($this[$key]);
	}
	function __toString()
	{
		return http_build_uri($this);
	}
}
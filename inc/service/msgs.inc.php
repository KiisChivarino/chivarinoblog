<? //модуль сообщений
DEFINE('MSG01','<i>Ошибка валидации</i>: <b>Некорректно заполнены одно или несколько полей!</b>');
DEFINE('MSG02','<i>Ошибка обработки данных:</i>: <b>Не удалось сохранить данные!</b>');
DEFINE('MSG03','<b>Заявка успешно отправлена на указанный Вами email!</b>');
DEFINE('MSG04','<b>Письмо отправить не удалось!</b>');
DEFINE('MSG05', '<i>Ошибка регистрации:</i><b>Не удалось получить пароль пользователя<b>');
DEFINE('MSG06','<b>Не удалось удалить пользователя из базы!</b>');
DEFINE('MSG07','<b>Не удалось удалить пользователя из файла!</b>');
DEFINE('MSG08','<b>Не удалось сменить статус пользователя!</b>');
DEFINE('MSG09','<b>Не удалось получить email пользователя!</b>');
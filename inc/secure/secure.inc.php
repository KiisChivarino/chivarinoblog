<? //библиотека функций авторизации, сессий, хэширования, работы с файлом .htpasswd
define ('FILE_NAME', '.htpasswd');
function getHash($string, $salt,$iterationCount){
    for ($i = 0; $i < $iterationCount; $i++)
        $string = sha1($string.$salt);
    return $string;
}

function saveHash($user, $hash, $salt, $iteration){
    $str = "$user:$hash:$salt:$iteration\n";
    if(file_put_contents(FILE_NAME, $str, FILE_APPEND))
        return true;
    else
        return false;
}

function userExists($login){
    if(!is_file(FILE_NAME))
        return false;
    $users = file(FILE_NAME);
    foreach($users as $user){
        if(strpos($user,$login) !== false)
            return $user;
    }
    return false;
}

function userDrop($login){
	if(!is_file(FILE_NAME))
        return false;
    $users = file(FILE_NAME);
	$id=0;
    foreach($users as $user){
		if(strpos($user,$login) !== false){
			$fp=fopen(FILE_NAME,"w");
			for($i=0;$i<sizeof($users);$i++){
				if($i==$id){
					unset($users[$i]);
				}
			}
			fputs($fp,implode("",$users));
			fclose($fp);
			return true;
		}
		$id++;
    }
    return false;
}

function logOut(){
    session_destroy();
    header('Location: /login.php');
    exit;
}
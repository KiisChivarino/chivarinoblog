<?//модуль регистрации пользователя

$uName = $_POST['uname'];
$email = trim(strip_tags($_POST['email']));
$phone = trim(strip_tags($_POST['phone']));
$uOrganization = trim(strip_tags($_POST['organization']));
$password = trim(strip_tags($_POST['string']));

$salt = str_replace('=', '', base64_encode(md5(microtime() . '1FD37EAA5ED9425683326EA68DCD0E59')));
$iterationCount = 100; //здесь поменять на случайное трехзначное число
$result = '';
$user = $email;

if(empty($email) or empty($uName) or empty($password)) $msg = MSG01; //здесь должна быть серверная валидация формы
else{
    try{
        //добавляем данные в базу
        $result = $users->saveUser($email, $uName, $uOrganization, $phone);
        if(!$result) throw new Exception(MSG02);
        
        //добавляем данные в файл
        if(userExists($user)) throw new Exception("Пользователь $user уже существует. Выберите другое имя.");
        $result = getHash($password, $salt, $iterationCount);
        if(empty($result)) throw new Exception(MSG05);
        if(!saveHash($user, $result, $salt, $iterationCount)) throw new Exception('При записи хеша '. $result. ' произошла ошибка');
        
        //формируем ссылку подтверждения регистрации
        $salt = str_replace('=', '', base64_encode(md5('1FD37EAA5ED9425683326EA68DCD0E59')));
        $iterationCount = 100;
        $href = $_SERVER['HTTP_ORIGIN'].$_SERVER['PHP_SELF']."?v=".getHash($email,$salt,$iterationCount);
        
        //отправляем почту кандидату в пользователи
        /*$res = smtp_send_mail(
                            "smtp://me@kurokami.ru:lsd666999@mail.kurokami.ru:25",
                            "Здравствуйте, $uName! Вы подали заявку на регистрацию на сайте «Блог Интех».
                            Вы прислали нам следующие контактные данные:
                            Название организации - $uOrganization
                            Телефон - $phone
                            Эл. почта - $email
                            Для подтверждения регистрации, пожалуйста,пройдите по ссылке: 
                            $href
                            Это письмо было выслано автоматически. Пожалуйста, не отвечайте на него.",
                            null,
                            array(
                                "envelope" => array(
                                    "from" => "me@kurokami.ru",
                                    "to" => "$email",
                                    "subject" => "Регистрация на сайте «Блог Интех»"
                                )
                            )
        );
        if(!$res) throw new Exception(MSG04);*/
        echo 'Отправка писем на бесплатном хостинге beget запрещена. Для завершения регистрации перейдите по <a href="'.$href.'">ссылке.</a>';
        $msg = MSG03;
    }catch(Exception $e){
        $msg = $e;
    }
	//header('Location: login.php?sent=0');
}
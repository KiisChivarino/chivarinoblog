<?
$validCode = trim(strip_tags($_GET['v']));
$salt = str_replace('=', '', base64_encode(md5('1FD37EAA5ED9425683326EA68DCD0E59')));
$iterationCount = 100;
$usersArr = $users->getUsers();
if (empty($validCode)) echo 'Регистрация не подтверждена. Попробуйте пройти регистрацию повторно.';
else{
    foreach($usersArr as $u){
        $mailHash = getHash($u['email'],$salt,$iterationCount);
        if($mailHash==$validCode){
            //добавляем пользователя в файл .htpasswd, меняем статус
            $user = $u['email'];
            if(userExists($user)){  
                $status = '1';
                $result = $candidates->setStatusAuthor($u['id']);
                if (!$result) {
                    $msg = MSG08;
                    break;
                }
            }else{
                $msg = "Пользователь $user не найден.";
            }
            $msg = 'Регистрация подтверждена успешно!';
            //header("Location: $_SERVER['HTTP_ORIGIN']");
        }
    }
}
?>
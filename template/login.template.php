<html>
<head>
	<title>Авторизация</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<script src="\js\lib.inc.js"></script>
</head>
<body>
	<nav style="float: right;">
			<a href="#" onclick="openMe('loginForm','registrationForm');">Вход</a>
			<a href="#" onclick="openMe('registrationForm','loginForm');">Регистрация</a>
		<!--<a href="http://system/service/mail/send.php">Отправить письмо</a>-->
	</nav>
	<H1>Блог</H1>
	<div id="loginForm" style="display:none;">
		<h2><?= $title?></h2>
		<form action="<?= $_SERVER['REQUEST_URI']?>" method="post">
			<div>
				<label for="txtUser">Логин</label>
				<input id="txtUser" type="text" name="user" value="<?= $user?>" />
			</div>
			<div>
				<label for="txtString">Пароль</label>
				<input id="txtString" type="text" name="pw" />
			</div>
			<div>
				<button type="submit">Войти</button>
			</div>	
		</form>
	</div>
	<div id="registrationForm" style="display:none;">
		<h2>Регистрация</h2>
		<form action="<?= $_SERVER['REQUEST_URI']?>" method="POST">
			<div>
				<label for="regUser">Ваше имя:</label>
				<input id="regUser" type="text" name="uname"/>
			</div>
			<div>
				<label for="regOrganization">Название организации:</label>
				<input id="regOrganization" type="text" name="organization"/>
			</div>
			<div>
				<label for="regEmail">Ваш e-mail:</label>
				<input id="regEmail" type="text" name="email"/>
			</div>
			<div>
				<label for="regPhone">Ваш телефон:</label>
				<input id="regPhone" type="text" name="phone"/>
			</div>
            <div>
				<label for="passId">Пароль:</label>
				<input id="passId" type="password" name="string"/>
			</div>
			<div>
				<button type="submit">Отправить</button>
			</div>
		</form>
	</div>
	<?if($msg) echo $msg;	?>
<?php
include ($_SERVER['DOCUMENT_ROOT'].'/inc/news/get_news.inc.php');
?>
</body>
</html>
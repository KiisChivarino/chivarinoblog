<html>
<head>
	<title>Хеширование SHA-1</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <script src="/js/lib.inc.js"></script>
</head>

<body>
	<ul>
		<?if ($_SESSION['admin']) echo
		"<li><a href='index.php'>На главную</a></li>";?>
		<li><a href='index.php?logout'>Завершить сеанс</a></li>
	</ul>
<h1>Хеширование SHA-1</h1>
<?echo $msg;?>
<h3><?= $result?></h3>
<form action="<?= $_SERVER['PHP_SELF']?>" method="post" style="display:none">
	<div>
		<label for="txtUser">Логин</label>
		<input id="txtUser" type="text" readonly="readonly" name="user" value="<?= $user?>" style="width:40em"/>
	</div>
	<div>
		<label for="txtString">Пароль</label>
		<input id="txtString" type="text" name="string" value="<?= $string?>" style="width:40em"/>
	</div>
	<div>
		<label for="txtSalt">Соль</label>
		<input id="txtSalt" type="text" name="salt" value="<?= $salt?>"  style="width:40em"/>
	</div>	
	<div>
		<label for="txtIterationCount">Число иттераций</label>
		<input id="txtIterationCount" type="text" name="n" value="<?= $iterationCount?>"  style="width:4em"/>
	</div>
	<div>
		<button type="submit">Создать</button>
	</div>	
</form>

    <table width="100%">
        <tr>
            <th>Подавшие заявку на регистрацию</th>
            <th>Зарегистрированные</th>
        </tr>
        <tr>
            <td><?getUsers(0, $candidates);?></td>
            <td><?getUsers(1, $authors);?></td>
        </tr>
    </table>
</body>
</html>
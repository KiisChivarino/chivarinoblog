<?php //класс новостей, методы взаимодействия с базой данных
//require "Db.class.php";
class NewsDB extends Db{
	
	function __construct(){
		parent::__construct();
	}
	function __destruct(){
		parent::__destruct();
	}
	
	public function getNews(){
		try{
			$sql = "SELECT msgs.id as id, title, category.name as category, description, userId, datetime 
					FROM msgs, category
					WHERE category.id = msgs.category
					ORDER BY msgs.id DESC";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch(Exception $e){
			return false;
		}
	}
	
	function saveNews($title, $category, $description, $userId){
		$dt = time();
		$sql = "INSERT INTO msgs(title, category, description, datetime, userId)
					VALUES('$title', $category, '$description', $dt, $userId)";
		$ret = $this->_db->exec($sql);
		if(!$ret)
			return false;
		return true;	
	}	
	
	public function deleteNews($id){
		try{
			$sql = "DELETE FROM msgs WHERE id = $id";
			$result = $this->_db->exec($sql);
			if (!$result) 
				throw new Exception($this->_db->lastErrorMsg());
			return true;
		}catch(Exception $e){
			echo $e->getMessage();
			return false;
		}
	}
    
	public function updateNews($id, $title, $category, $description){
        try{
			$sql = "UPDATE msgs SET title = '$title', category=$category, description='$description' WHERE id = $id";
			$result = $this->_db->exec($sql);
			if (!$result) 
				throw new Exception($this->_db->lastErrorMsg());
			return true;
		}catch(Exception $e){
			echo $e->getMessage();
			return false;
		}
    }
}
?>
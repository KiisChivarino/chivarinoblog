<?php
//require('Db.class.php');
class UserDb extends Db{	
	function __construct(){
		parent::__construct();
	}
	
	function __destruct(){
		parent::__destruct();
	}
	
	function getUsers(){
		try{
			$sql = "SELECT id, email, uName, uOrganization, phone FROM user";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
	}
	
	function saveUser($email, $uName, $uOrganization, $phone){
		$dt = time();
        $sql = "INSERT INTO user(email, uName, uOrganization, phone, dtReg)
					VALUES('$email', '$uName', '$uOrganization', '$phone', $dt)";
		$ret = $this->_db->exec($sql);
		if(!$ret)
			return false;
		return true;
	}
	
	function deleteUser($id){
		try{
			$sql = "DELETE FROM user WHERE id = $id";
			$result = $this->_db->exec($sql);
			if (!$result) 
				throw new Exception($this->_db->lastErrorMsg());
			return true;
		}catch(Exception $e){
			echo $e->getMessage();
			return false;
		}
	}
	
	function updateUser($id){
        try{    
            $sql = "UPDATE user SET uName=$uName, uOrganization=$uOrganization, phone=$phone";
            $result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
    }
	
	public function getUserMail($id){
		try{
			$sql = "SELECT email FROM user WHERE id=$id";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			$result = $this->db2Arr($result);
			return $result[0]['email'];
		}catch (Exception $e){
			return false;
		}
	}
    public function getUserStatus($email){
        try{
			$sql = "SELECT status FROM user WHERE email='$email'";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			$result = $this->db2Arr($result);
			return $result[0]['status'];
		}catch (Exception $e){
			return false;
		}
    }
}

class AuthorUserDb extends UserDb{
    function getAuthors(){
        try{
			$sql = "SELECT id, email, uName, uOrganization, phone FROM user WHERE status=1";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
    }
    function getAuthorByLogin($login){
        try{
			$sql = "SELECT id, uName FROM user WHERE email='$login'";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
    }
    
    function getAuthorById($id){
        try{
			$sql = "SELECT uName FROM user WHERE id=$id";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
    }
}

class CandidateUserDb extends UserDb{
    function getCandidates(){
        try{
			$sql = "SELECT id, email, uName, uOrganization, phone FROM user WHERE status = '0'";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
    }
    
    function setStatusAuthor($id){
        try{
            $sql = "UPDATE user SET status = 1 WHERE id = $id";
            $result = $this->_db->query($sql);
            if (!$result) 
				throw new Exception($this->_db->lastErrorMsg());
			return true;
        }catch (Exception $e){
			return false;
		}
    }
}
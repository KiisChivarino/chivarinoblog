<? class CandidateUserDb extends UserDb{
    function getCandidates(){
        try{
			$sql = "SELECT id, email, uName, uOrganization, phone FROM user WHERE status = '0'";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch (Exception $e){
			return false;
		}
    }
    
    function setStatusAuthor($id){
        try{
            $sql = "UPDATE user SET status = 1 WHERE id = $id";
            $result = $this->_db->query($sql);
            if (!$result) 
				throw new Exception($this->_db->lastErrorMsg());
			return true;
        }catch (Exception $e){
			return false;
		}
    }
}
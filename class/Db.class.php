<?php 
class Db implements IteratorAggregate{
	
	const DB_NAME = 'news.db';
	protected $_db;
	protected $_items;
	
	protected function __construct(){
		if(is_file(self::DB_NAME)){
			$this->_db = new SQLite3(self::DB_NAME);
		}else{
			$this->_db = new SQLite3(self::DB_NAME);
			$sql = "CREATE TABLE msgs(
									id INTEGER PRIMARY KEY AUTOINCREMENT,
									title TEXT,
									category INTEGER,
									description TEXT,
									userId INTEGER DEFAULT 0,
									datetime INTEGER
								)";
			$this->_db->exec($sql) or $this->_db->lastErrorMsg();
			$sql = "CREATE TABLE category(
										id INTEGER PRIMARY KEY AUTOINCREMENT,
										name TEXT
									)";
			$this->_db->exec($sql) or $this->_db->lastErrorMsg();
			$sql = "INSERT INTO category(id, name)
						SELECT 1 as id, 'Сайт компании Интех' as name
						UNION SELECT 2 as id, 'Сайт магазина Webintech' as name
                        UNION SELECT 3 as id, 'Сайт веб-студии Redline' as name
                        UNION SELECT 4 as id, 'Блог Webintech' as name
						UNION SELECT 5 as id, 'IT-технологии' as name";
			$this->_db->exec($sql) or $this->_db->lastErrorMsg();
			$sql = "CREATE TABLE user(
						id INTEGER PRIMARY KEY AUTOINCREMENT,
						email TEXT,
						uName TEXT,
						uOrganization TEXT,
						phone TEXT,
						status INTEGER DEFAULT 0,
                        dtReg INTEGER
			)";
			$this->_db->exec($sql) or $this->_db->lastErrorMsg();
            			$sql = "CREATE TABLE problem(
						id INTEGER PRIMARY KEY AUTOINCREMENT,
						title TEXT,
						description TEXT,
						dtCreate INTEGER,
						dtMustBegin INTEGER,
                        dtMustEnd INTEGER,
                        dtRealBegin INTEGER,
                        dtRealEnd INTEGER,
						status INTEGER DEFAULT 0,
                        report TEXT,
                        parentProblemId INTEGER,
                        userId INTEGER
			)";/*0 - не приступал, 1 - выполняется, 2 - пауза, 3 - выполнено успешно, 4 - отмена, 5 - провал*/
			$this->_db->exec($sql) or $this->_db->lastErrorMsg();
		}
		$this->getCategory();
	}
	
	function getIterator(){
		return new ArrayIterator($this->_items);
	}
	
	protected function getCategory(){
		$sql = "SELECT id, name FROM category";
		$result = $this->_db->query($sql);
		while ($r = $result->fetchArray(SQLITE3_ASSOC))
			$this->_items[$r['id']] = $r['name'];
	}
	
	function __destruct(){
		unset($this->_db);
	}
	
	protected function db2Arr($data){
		$arr = array();
		while($row = $data->fetchArray(SQLITE3_ASSOC))
			$arr[] = $row;
		return $arr;
	}
	
	function clearData($data){
		return $this->_db->escapeString($data); 
	}
	
}
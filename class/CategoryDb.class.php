<?php 
class CategoryDb extends Db{
    function __construct(){
		parent::__construct();
	}
	function __destruct(){
		parent::__destruct();
	}
    public function getCategory(){
        try{
			$sql = "SELECT id, name FROM category";
			$result = $this->_db->query($sql);
			if (!is_object($result)) 
				throw new Exception($this->_db->lastErrorMsg());
			return $this->db2Arr($result);
		}catch(Exception $e){
			return false;
		}
    }
    public function deleteCategory(){
        try{
			$sql = "DELETE FROM category WHERE id = $id";
			$result = $this->_db->exec($sql);
			if (!$result) 
				throw new Exception($this->_db->lastErrorMsg());
			return true;
		}catch(Exception $e){
			echo $e->getMessage();
			return false;
		}
    }
    public function saveCategory($name){
		$dt = time();
		$sql = "INSERT INTO category(name)
					VALUES('$name')";
		$ret = $this->_db->exec($sql);
		if(!$ret)
			return false;
		return true;	
	}
}
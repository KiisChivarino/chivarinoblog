<?php //главная страница для зарегистрированного пользователя
include_once ($_SERVER['DOCUMENT_ROOT'].'/inc/service/msgs.inc.php');

//сессия пользователя >>>
require_once ($_SERVER['DOCUMENT_ROOT'].'/inc/secure/session.inc.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/inc/secure/secure.inc.php');

if(isset($_GET['logout']))
    logOut();
//<<<сессия пользователя

//подключаем классы для работы с БД (поменять на autoload)
require ($_SERVER['DOCUMENT_ROOT'].'/class/Db.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/class/NewsDB.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/class/UserDB.class.php');
//require ($_SERVER['DOCUMENT_ROOT'].'/class/AuthorUserDB.class.php');

//создаем объект для работы с БД
$news = new NewsDB;
$authors = new AuthorUserDb;
$authorArr = $authors->getAuthorByLogin($_SESSION['login']);
if($_SESSION['admin']) $currUserName =  'Администратор';
elseif ($_SESSION['user']) $currUserName = $authorArr[0]['uName'];
$errMsg = '';

//подключаем скрипты для операций с новостями (изменение, добавление, удаление)
if($_SERVER['REQUEST_METHOD']=='POST'){
    if (isset($_POST['artId']))
        include ($_SERVER['DOCUMENT_ROOT'].'/inc/news/update_news.inc.php');
    else 
        include ($_SERVER['DOCUMENT_ROOT'].'/inc/news/save_news.inc.php');
}

if(isset($_GET['del']) && is_numeric($_GET['del']))
	include ($_SERVER['DOCUMENT_ROOT'].'/inc/news/delete_news.inc.php');
?>
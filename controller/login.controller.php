<? //гостевая страница с формами регистрации и авторизации

include_once ($_SERVER['DOCUMENT_ROOT'].'/inc/service/msgs.inc.php');

require_once ($_SERVER['DOCUMENT_ROOT'].'/class/Db.class.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/class/NewsDB.class.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/class/UserDB.class.php');
//require ($_SERVER['DOCUMENT_ROOT'].'/class/CandidateUserDB.class.php');
//require ($_SERVER['DOCUMENT_ROOT'].'/class/AuthorUserDB.class.php');

$news = new NewsDb;
$users = new UserDb;
$candidates = new CandidateUserDB;
$authors = new AuthorUserDb;
$title = 'Авторизация';
$user  = '';
$msg = '';


require_once ($_SERVER['DOCUMENT_ROOT'].'/inc/secure/secure.inc.php');
session_start();
header("HTTP/1.0 401 Unauthorized");

//logOut();
if($_SERVER['REQUEST_METHOD']=='POST'){
	if (isset($_POST['pw']))
		include_once ($_SERVER['DOCUMENT_ROOT'].'/inc/secure/user_login.inc.php');
	if (isset($_POST['email'])){
		include_once($_SERVER['DOCUMENT_ROOT'].'/inc/secure/user_reg.inc.php');
	}
}
if($_SERVER['REQUEST_METHOD']=='GET' && isset($_GET['v'])){
    include_once($_SERVER['DOCUMENT_ROOT'].'/inc/secure/user_validate.inc.php');
}

?>
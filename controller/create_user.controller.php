<?
/*Страница управления пользователями*/
include ($_SERVER['DOCUMENT_ROOT'].'/inc/service/msgs.inc.php');

//сессия пользователя >>>
require_once ($_SERVER['DOCUMENT_ROOT'].'/inc/secure/session.inc.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/inc/secure/secure.inc.php');
//<<<сессия пользователя

//подключаем классы для работы с БД (поменять на autoload)
require ($_SERVER['DOCUMENT_ROOT'].'/class/Db.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/class/UserDB.class.php');
//require ($_SERVER['DOCUMENT_ROOT'].'/class/CandidateUserDB.class.php');
//require ($_SERVER['DOCUMENT_ROOT'].'/class/AuthorUserDB.class.php');
require ($_SERVER['DOCUMENT_ROOT'].'/inc/user/get_users.inc.php');

//создаем объекты для манипуляциями пользователями
$candidates = new CandidateUserDB;
$authors = new AuthorUserDB;
$msg = '';

$user = 'root';
$string = '1234';
$salt = '';
$iterationCount = 100;
$result = '';

if (!$salt)
	$salt = str_replace('=', '', base64_encode(md5(microtime() . '1FD37EAA5ED9425683326EA68DCD0E59')));
if ($_SERVER['REQUEST_METHOD']=='POST'){
	$user = $_POST['user'] ?: $user;
	if(!userExists($user)){  
        $id = $_POST['status'] * 1;
        if($id === 0)
            $msg = "Необходимо добавлять пользователей из подавших заявку!";
        else{
            $result = $candidates->setStatusAuthor($id);
            if (!$result) 
                $msg = MSG08;
            else{        
                $string = $_POST['string'] ?: $string;
                $salt = $_POST['salt'] ?: $salt;
                $iterationCount = (int) $_POST['n'] ?: $iterationCount;
                $result = getHash($string, $salt, $iterationCount);
                if(saveHash($user, $result, $salt, $iterationCount))
                    $result = 'Хеш '. $result. ' успешно добавлен в файл';
                else
                    $result = 'При записи хеша '. $result. ' произошла ошибка';
            }
        }
    }else{
		$result = "Пользователь $user уже существует. Выберите другое имя.";
	}
}
if(isset($_GET['del']) && is_numeric($_GET['del']))
	include ($_SERVER['DOCUMENT_ROOT'].'/inc/user/delete_user.inc.php');
?>
// переключает видимость между двумя элементами
function openMe(openEl,closeEl){
	if (!openEl) return false;
	else {
		openEl = document.getElementById(openEl);
		openEl.style.display = "block";
		
		if (closeEl){
			openEl = document.getElementById(closeEl);
			openEl.style.display = "none";
		}
	}
}

//открывает-закрывает текст статьи
function openDesc(el){
    var artEl = el.parentNode.parentNode;
    var dscEl = artEl.getElementsByClassName('dsc')[0];
    dscEl.style.display=='none' ? dscEl.style.display = '' : dscEl.style.display = 'none';
}

//готовит статью к редактированию
function goToUpdate(el){
    
    var formEl = document.forms[0];

    //меняем заголовок формы и заголовок submit
    var newsTitle = document.getElementById('newsTitle');
    var newNewsTitle = newsTitle.firstChild.nodeValue.replace('Добавить','Редактировать');
    newsTitle.firstChild.nodeValue = newNewsTitle;
    formEl.submitName.value = "Редактировать!";
    
    //добавляем в форму input с id статьи
    var artId = el.getAttribute('id');
    var idEl = document.createElement('input');
    idEl.setAttribute('type','hidden');
    idEl.setAttribute('name', 'artId');
    idEl.setAttribute('value', artId);
    formEl.appendChild(idEl);

    //вытаскиваем данные из статьи    
    var artEl = el.parentNode.parentNode;
    var ttlData = artEl.getElementsByClassName('ttl')[0].firstChild.firstChild.nodeValue;
    var dscData = artEl.getElementsByClassName('dsc')[0].textContent;
	var ctgData = artEl.getElementsByClassName('ctg')[0].firstChild.nodeValue;

    //заносим данные в форму
    formEl.title.value = ttlData;
    formEl.description.value = dscData;
    var options = formEl.category.getElementsByTagName('option');
    for (var i in options){
        if (options[i].firstChild.nodeValue == ctgData){
            options[i].setAttribute('selected','selected');
            break;
        }
    }
}
//меняет пробелы и табуляции на &nbsp для отступов в начале строки
function spaceTabToNbsp(){
    
    var dscData = document.forms[0].description.value;
    var entersAndSpacesArr = dscData.match(/\n[ \t]+/g);
    var countSpaces = 0;
    
    for (var i in entersAndSpacesArr){
        countSpaces = entersAndSpacesArr[i].length-1;
        dscData = dscData.replace(entersAndSpacesArr[i],'\n'+getNbsps(countSpaces));
    }
    
    document.forms[0].description.value = dscData;
    
    function getNbsps(countSpaces){
        var str = '';
        for (var i=0; i<countSpaces;i++){
            str+='&nbsp';
        }
        return str;
    }
}

function setStatus(el){
    var id = el.id;
    var email = el.getAttribute('email');
    var form = document.forms[0];
    var hiddenId = document.createElement('input');
    hiddenId.setAttribute('type','hidden');
    hiddenId.setAttribute('name','status');
    hiddenId.setAttribute('value',id);
    form.appendChild(hiddenId);
    form.user.value = email;
    form.style.display = '';
}